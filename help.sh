help() {
    echo -ne " 
-help : qui indiquera les commandes que vous pouvez utiliser  
-ls : lister des fichiers et les dossiers visible comme caché rm : supprimer un fichier  
-rmd ou rmdir : supprimer un dossier  
- about : une description de votre programme  
- version ou --v ou vers : affiche la version de votre prompt  
- age : vous demande votre âge et vous dit si vous êtes majeur ou mineur  
- quit : permet de sortir du prompt  
- profil : permet d’afficher toutes les informations sur vous même.  
- First Name, Last name, age, email  
- passw : permet de changer le password avec une demande de confirmation  
- cd : aller dans un dossier que vous venez de créer ou de revenir à un dossier précédent 
- pwd : indique le répertoire actuelle courant  
- hour : permet de donner l’heure actuelle  
- * : indiquer une commande inconnu  
- httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique. Votre prompt doit vous demander quel sera le nom du fichier.  
- smtp : vous permet d’envoyer un mail avec une adresse un sujet et le corp du mail  
- open : ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas  

"
}