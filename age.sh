# - age : vous demande votre âge et vous dit si vous êtes majeur ou mineur
age() {
    echo "How old are you?"
    read uAge

    if ! [[ "$uAge" =~ ^[0-9]+$ ]]; then
        echo "error : Enter a correct number (positive, no letters)";
    elif [ $uAge -ge 18 ]; then
        echo "You are an adult"
    else 
        echo "You are not an adult"
    fi
}