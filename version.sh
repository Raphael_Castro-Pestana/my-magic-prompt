# - version ou --v ou vers : affiche la version de votre prompt
version() {
  echo $(git tag | tail -n 1)
}