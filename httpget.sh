# httpget : permet de télécharger le code source html d’une page web et de
# l’enregistrer dans un fichier spécifique. Votre prompt doit vous demander quel sera le
# nom du fichier.

httpget() {
    if [ $# -gt 2 ]; then
        echo -e "httpget : too many argugments\nTry 'help' for more information."
    elif [ $# -lt 2 ]; then
        echo "httpget: missing URL operand"
    elif [[ $(curl $2) ]]; then
        echo "Enter a file name:"
        read fileName
        curl $2 > "$fileName"".txt"
        else 
            echo $2
            echo "httpget : invalid URL operand"
    fi    
}