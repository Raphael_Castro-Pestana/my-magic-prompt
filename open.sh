# - open : ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas

open() {
    case "$#" in
    1 ) echo "open: missing file operand";;
    2 ) vim $2;;
    * ) echo "too many arguments";;
  esac
}
