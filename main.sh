#!/bin/bash
source help.sh
source existingCmd.sh
source about.sh
source version.sh
source age.sh
source quit.sh
# source profil.sh
# source passw.sh
source hour.sh
source httpget.sh
# source smtp.sh
source open.sh


cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    quit | q | exit ) quit;;
    help ) help;;
    ls ) existingCmd $*;;
    rm ) existingCmd $*;;
    rmdir ) existingCmd $*;;
    cd ) existingCmd $*;;
    pwd ) existingCmd $*;;
    about ) about;;
    version ) version;;
    hour ) hour;;
    age ) age;;
    httpget ) httpget $*;;
    open ) open $*;;
    * ) echo "unknown command";;
  esac
}

main() {
  lineCount=1

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[31m${lineCount}\033[m] - \033[33mRAPHOU\033[m ~ 🕹️ ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
